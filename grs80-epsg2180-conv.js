"use strict";

var Math = require("mathjs");

/*
Wersja Javascript:
Autor: Tomasz Marcinkowski
E-mail: tomasz.marcinkowski@dev37.net
Data modyfikacji: 2017-12-25
Uwagi: oprogramowanie darmowe na tych samych zasadach co wersja oryginalna (patrz nizej).

Usuniete informacje dydaktyczne - zagadnienia teoretyczne mozna znalezc w artykule linkowanym w notce ponizej, 
w artykule Wikipedii https://pl.wikipedia.org/wiki/Odwzorowanie_Gaussa-Kr%C3%BCgera oraz w oryginalnym 
pakiecie oprogramowania dostępnym tu: http://ecdis.republika.pl/page6434192505427b12099241.html

Oryginalny kod w języku C:
---------------------------------------
Autor: Zbigniew Szymanski
E-mail: z.szymanski@szymanski-net.eu
Wersja: 1.1
Historia zmian:
		1.1 dodano przeksztalcenie odwrotne PUWG 1992 ->WGS84
		1.0 przeksztalcenie WGS84 -> PUWG 1992
Data modyfikacji: 2012-11-27
Uwagi: Oprogramowanie darmowe. Dozwolone jest wykorzystanie i modyfikacja 
       niniejszego oprogramowania do wlasnych celow pod warunkiem 
       pozostawienia wszystkich informacji z naglowka. W przypadku 
       wykorzystania niniejszego oprogramowania we wszelkich projektach
       naukowo-badawczych, rozwojowych, wdrozeniowych i dydaktycznych prosze
       o zacytowanie nastepujacego artykulu:

       Zbigniew Szymanski, Stanislaw Jankowski, Jan Szczyrek, 
       "Reconstruction of environment model by using radar vector field histograms.",
       Photonics Applications in Astronomy, Communications, Industry, and 
       High-Energy Physics Experiments 2012, Proc. of SPIE Vol. 8454, pp. 845422 - 1-8,
       doi:10.1117/12.2001354

Literatura:
       Uriasz, J., "Wybrane odwzorowania kartograficzne", Akademia Morska w Szczecinie,
       http://uriasz.am.szczecin.pl/naw_bezp/odwzorowania.html
-----------------------------------------
*/

module.exports.wgs84_to_puwg92 = 
module.exports.grs80_to_epsg2180 = 
module.exports.d2m = function(B, L) {
/*
Opis:
    konwersja wspolrzednych z ukladu WGS 84 do ukladu PUWG 1992
Parametry:
    B - szerokosc geograficzna wyrazona w stopniach
    L - dlugosc geograficzna wyrazona w stopniach
Zwracana wartosc:
    (X, Y) - konwersja powiodla sie, gdzie
      X - wspolrzedna pionowa(!) X ukladu PUWG 1992
      Y - wspolrzedna pozioma(!) Y ukladu PUWG 1992
    lub
    (null, null) - szerokosc lub dlugosc geograficzna poza zakresem
*/

    // Parametry elipsoidy GRS-80
    var e     =        0.0818191910428;
                                 // pierwszy mimosrod elipsoidy
    var R0    =  6367449.14577;  // promien sfery Lagrange'a
    var Snorm =        2.0e-6;   // parametr normujacy
    var xo    =  5760000.0;      // parametr centrujacy

    //Wspolczynniki wielomianu
    var a0 = 5765181.11148097;
    var a1 =  499800.81713800;
    var a2 =     -63.81145283;
    var a3 =       0.83537915;
    var a4 =       0.13046891;
    var a5 =      -0.00111138;
    var a6 =      -0.00010504;

    // Parametry odwzorowania Gaussa-Kruegera dla ukladu PUWG92
    var L0 =      19.0; // Poczatek ukladu wsp. PUWG92 (dlugosc)
    var m0 =       0.9993;
    var x0 =-5300000.0;
    var y0 =  500000.0;

    // Zakres stosowalnosci metody
    var Bmin  = 48.0*Math.PI/180.0;
    var Bmax  = 56.0*Math.PI/180.0;
    var dLmin = -6.0*Math.PI/180.0;
    var dLmax =  6.0*Math.PI/180.0;

    // Weryfikacja danych wejsciowych
    var Brad  = B*Math.PI/180.0;
    var dL    = L-L0;
    var dLrad = dL*Math.PI/180.0;

    if ((Brad<Bmin) || (Brad>Bmax)
       || (dLrad<dLmin) || (dLrad>dLmax))
          return [null, null];

    // etap I - elipsoida na kule
    var U        = 1.0-e*Math.sin(Brad);
    var V        = 1.0+e*Math.sin(Brad);
    var K        = Math.pow((U/V),(e/2.0));
    var C        = K*Math.tan(Brad/2.0+Math.PI/4.0);
    var fi       = 2.0*Math.atan(C)-Math.PI/2.0;
    var d_lambda = dLrad;

    // etap II - kula na walec
    var p     = Math.sin(fi);
    var q     = Math.cos(fi)*Math.cos(d_lambda);
    var r     = 1.0+Math.cos(fi)*Math.sin(d_lambda);
    var s     = 1.0-Math.cos(fi)*Math.sin(d_lambda);
    var Xmerc = R0*Math.atan(p/q);
    var Ymerc = 0.5*R0*Math.log(r/s);

    // etap III - walec na plaszczyzne
    var Z = Math.complex((Xmerc-xo)*Snorm, Ymerc*Snorm);
    var Z5 = Math.add(a5, Math.multiply(Z, a6));
    var Z4 = Math.add(a4, Math.multiply(Z, Z5));
    var Z3 = Math.add(a3, Math.multiply(Z, Z4));
    var Z2 = Math.add(a2, Math.multiply(Z, Z3));
    var Z1 = Math.add(a1, Math.multiply(Z, Z2));
    var Zgk = Math.add(a0, Math.multiply(Z, Z1));
    var Xgk = Zgk.re;
    var Ygk = Zgk.im;

    // Przejscie do ukladu aplikacyjnego
    var X = m0*Xgk+x0;
    var Y = m0*Ygk+y0;

    return [X, Y];
}

module.exports.puwg92_to_wgs84 = 
module.exports.epsg2180_to_grs80 = 
module.exports.m2d = function (X, Y) {
/*
Opis:
    konwersja wspolrzednych z ukladu PUWG 1992 do ukladu WGS 84
Parametry:
    X - wspolrzedna pionowa (!) X ukladu PUWG 1992
    Y - wspolrzedna pozioma (!) Y ukladu PUWG 1992
Zwracana wartosc:
    (B, L)
    gdzie
      B - szerokosc geograficzna wyrazona w stopniach
      L - dlugosc geograficzna wyrazona w stopniach
*/

    var L0 =      19.0; //Poczatek ukladu wsp. PUWG92 (dlugosc)
    var m0 =       0.9993;
    var x0 =-5300000.0;
    var y0 =  500000.0;

    var R0      = 6367449.14577;    //promien sfery Lagrange'a
    var Snorm   =       2.0e-6;     //parametr normujacy
    var xo_prim = 5765181.11148097; //parametr centrujacy

    // Wspolczynniki wielomianu
    var b0 = 5760000;
    var b1 =  500199.26224125;
    var b2 =      63.88777449;
    var b3 =      -0.82039170;
    var b4 =      -0.13125817;
    var b5 =       0.00101782;
    var b6 =       0.00010778;

    // Wspolczynniki szeregu trygonometrycznego
    var c2 = 0.0033565514856;
    var c4 = 0.0000065718731;
    var c6 = 0.0000000176466;
    var c8 = 0.0000000000540;

    // Przejscie z ukladu aplikacyjnego
    var Xgk, Ygk;
    Xgk = (X-x0)/m0;
    Ygk = (Y-y0)/m0;

    // etap I - plaszczyzna na walec
    var Z = Math.complex((Xgk-xo_prim)*Snorm, Ygk*Snorm);
    var Z5 = Math.add(b5, Math.multiply(Z, b6));
    var Z4 = Math.add(b4, Math.multiply(Z, Z5));
    var Z3 = Math.add(b3, Math.multiply(Z, Z4));
    var Z2 = Math.add(b2, Math.multiply(Z, Z3));
    var Z1 = Math.add(b1, Math.multiply(Z, Z2));
    var Zmerc = Math.add(b0, Math.multiply(Z, Z1));

    var Xmerc = Zmerc.re;
    var Ymerc = Zmerc.im;

    // etap II - walec na kule
    var alfa = Xmerc/R0;
    var beta = Ymerc/R0;

    var w        = 2.0*Math.atan(Math.exp(beta))-Math.PI/2.0;
    var fi       = Math.asin(Math.cos(w)*Math.sin(alfa));
    var d_lambda = Math.atan(Math.tan(w)/Math.cos(alfa));

    // etap III - kula na elipsoide
    var Brad  = fi+c2*Math.sin(2.0*fi)+c4*Math.sin(4.0*fi)+c6*Math.sin(6.0*fi)+c8*Math.sin(8.0*fi);
    var dLrad = d_lambda;

    // Obliczenia koncowe
    var B  = Brad/Math.PI*180.0;
    var dL = dLrad/Math.PI*180.0;
    var L  = dL+L0;

    return [B, L];
}


